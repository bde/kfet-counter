#!/usr/bin/env python
import os

from flask import Flask, redirect, render_template, request

app = Flask(__name__)

def get_opened_value() -> bool:
    """
    Retrieve the interrupter value.
    """
    with open("inter.txt") as file:
        return bool(int(file.read().strip().replace("\n", "")))

def set_opened_value(value: int) -> None:
    """
    Store the value of the interrupter.
    """
    with open("inter.txt", "w") as file:
        file.write(str(value))

def get_counter_value() -> int:
    """
    Retrieve the counter value.
    """
    with open("counter.txt") as file:
        return int(file.read().strip().replace("\n", ""))


def set_counter_value(value: int) -> None:
    """
    Store the counter value.
    """
    with open("counter.txt", "w") as file:
        file.write(str(value))


@app.route('/')
def display_counter():
    """
    Display the main page with the counter.
    If ?admin=i is in the URL, dipslay admin buttons.
    """
    value = get_counter_value()
    state = get_opened_value()
    admin = request.args.get("admin", type=bool, default=False)
    return render_template("display.html", counter_value=value, state=state, admin=admin)


@app.route('/inc')
def inc_counter():
    """
    Increment the counter and redirect to the main page.
    """
    set_counter_value(get_counter_value() + 1)
    return redirect('/?admin=1')


@app.route('/dec')
def dec_counter():
    """
    Decrement the counter and redirect to the main page.
    """
    set_counter_value(max(get_counter_value() - 1, 0))
    return redirect('/?admin=1')

@app.route('/closek')
def close_kfet():
    """
    Display that Kfet is closed
    """
    set_opened_value(0)
    set_counter_value(0)
    return redirect('/?admin=1')

@app.route('/openk')
def open_kfet():
    """
    Display that Kfet is opened
    """
    set_opened_value(1)
    set_counter_value(1)
    return redirect('/?admin=1')

if __name__ == '__main__':
    # Init file
    if not os.path.isfile("counter.txt"):
        with open("counter.txt", "w") as f:
            f.write("0")
    if not os.path.isfile("inter.txt"):
        with open("inter.txt", "w") as f:
            f.write("0")

    # Start webserver
    app.run(host="0.0.0.0", port=5000, debug=os.getenv("FLASK_DEBUG", False))
