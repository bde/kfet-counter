# Compteur de personnes présentes à la Kfet

Les temps sont durs avec la crise sanitaire de la COVID-19,
le BDE se voit dans l'obligation de réguler les entrées à la Kfet.
Pas plus de 30 personnes en simultanées ne sont tolérées.

Afin d'éviter de venir pour rien, un compteur est disponible à l'adresse
[kfet.crans.org](kfet.crans.org)
(pour l'instant [kfet.ynerant.fr](kfet.ynerant.fr)) afin de voir
si la Kfet est libre ou non.

# Installation

## Sur un environnement de développement

Il suffit d'installer Flask dans un environnement virtuel Python :

```bash
$ sudo apt-get install python3-venv
$ python3 -m venv venv
$ . venv/bin/activate
(venv) $ pip install flask
```

Une fois dans l'environnement virtuel avec Flask installé, il suffit de lancer
`./app.py` pour démarrer le serveur local. Ouvrez votre navigateur,
rendez-vous sur la page [127.0.0.1:5000](http://127.0.0.1:5000), enjoy :)

## Avec Docker

Un environnement de développement se construit facilement avec Docker :

```bash
docker build -t kfetcounter .
docker run -p 5000:5000 -v $(pwd):/app -e FLASK_DEBUG=1 kfetcounter
```

Ou avec Docker-compose :

```yaml
services:
  kfet-counter:
    build: https://gitlab.crans.org/bde/kfet-counter.git
    restart: always
    volumes:
      - $(pwd):/app
    environment:
      - FLASK_DEBUG=1
    ports:
      - 5000:5000
```

## Sur un serveur de production

En cours ...
