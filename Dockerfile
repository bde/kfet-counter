FROM python:3.9-alpine
RUN pip install flask
COPY . /app
WORKDIR /app
EXPOSE 5000
ENTRYPOINT ["/app/app.py"]
